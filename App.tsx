import { FlatList, Image, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useEffect, useState } from 'react';

const Home = () => {

  const [data, setData] = useState([])

  const loadData = async () => {
    const response = await fetch('https://rickandmortyapi.com/api/character')
    if (response.status === 200) {
      const responseJson = await response.json()
      setData(responseJson?.results)
    }
  }

  useEffect(() => {
    loadData()
  }, [])

  return (
    <View>
      <FlatList
        data={data}
        renderItem={({ item }) => {
          const { name, status, species, image }: any = item
          return (
            <View style={{ marginVertical: 20, alignItems: 'center' }}>
              <Image source={{ uri: image }} width={150} height={150} />
              <Text>{name}</Text>
              <Text>Specie:{species}</Text>
              <Text>Status:{status}</Text>
            </View>
          )
        }}
      />


    </View>
  )
}
const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
